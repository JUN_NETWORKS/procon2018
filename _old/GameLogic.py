# othello/OthelloLogic.py をプロコン用にしたやつ

import numpy as np
from numba.decorators import jit  # 高速化ライブラリ


# 敵の初期座標を自分の初期座標を元に作成
def generate_enemy_position(field_size=(8, 11), me_1=(1, 1), me_2=(6, 9)):
    enemy_1 = (me_1[0], field_size[1] - me_1[1] - 1)
    enemy_2 = (me_2[0], field_size[1] - me_2[1] - 1)

    return enemy_1, enemy_2


# tuple同士の足し算. 座標計算で使う
def plus_tuple(tuple1, tuple2):
    return (tuple1[0] + tuple2[0], tuple1[1] + tuple2[1])


# 移動 -> 行動後の座標を返す
# 削除 -> エージェント自体は動かないので現在と同じ座標を返す
def move_or_delete(command, current, apply):
    if command == 1:
        return plus_tuple(current, apply)
    else:
        return current


# 領域ポイント計算の探索処理
@jit
def mask_process(mask, max_row, max_column):
    # record0 == record1 になる(領域の場所の確定)までループ
    record0, record1 = 0, 1
    equal = 0  # 前回と同じなら +1 する
    while equal < 15:
        record0 = record1
        for i in range(1, max_row):
            for j in range(1, max_column):

                around = []  # ある座標の周り(上下左右)の値を入れるリスト
                if mask[i][j] == 0:
                    around.append(mask[i + 1][j])
                    around.append(mask[i - 1][j])
                    around.append(mask[i][j + 1])
                    around.append(mask[i][j - 1])

                    if -1 in around:  # -1が周りにあった場合
                        mask[i][j] = -1
                    else:
                        pass
                else:  # 空白以外は無視
                    continue
        record1 = np.sum(mask == 0)
        if record0 == record1:
            equal += 1
        else:
            equal = 0
    return mask


class Board():

    def __init__(self, score_field=np.random.randint(-2, 16, size=(8, 11)), me_1=(1, 1), me_2=(6, 9)):
        # Setup Board

        # 各マスの点数が入ってる
        self.score_field = score_field
        # どっちのチームがどのマスを持っているかの情報 1: me_1, 2: me_2, -1: enemy_1, -2: enemy_2, 0: none
        self.field = np.zeros(score_field.shape, dtype=int)

        # my team position
        self.me_1 = me_1
        self.me_2 = me_2
        # Genetate enemy position
        self.enemy_1, self.enemy_2 = generate_enemy_position(score_field.shape, me_1, me_2)

        # plot init position
        self.field[self.me_1[0]][self.me_1[1]] = 1
        self.field[self.me_2[0]][self.me_2[1]] = 2
        self.field[self.enemy_1[0]][self.enemy_1[1]] = -1
        self.field[self.enemy_2[0]][self.enemy_2[1]] = -2

    def countDiff(self, p=1):
        """
        :param p: プレイヤー番号: 1:me, -1: enemy 0: empty
        :return:
        """
        # インデックスの最大行と最大列
        max_row = self.field.shape[0] - 1
        max_column = self.field.shape[1] - 1

        # 自チーム
        if p==1:
            # タイルポイントの計算
            mask = np.where(self.field > 0)
            position_list = [(i, j) for i, j in zip(mask[0], mask[1])]
            my_tile_score = [self.score_field[i][j] for i, j in position_list]
            my_tile_score = np.sum(my_tile_score)

            # 領域ポイントの計算
            # 自チームのマスを1, それ以外を0とする
            mask = np.where(self.field > 0, 1, 0)
            # 4辺の自チーム以外のタイルを-1とする
            mask[[0, max_row], :] = np.where(self.field[[0, max_row], :] > 0, 1, -1)  # 0行目と最終行
            mask[:, [0, max_column]] = np.where(self.field[:, [0, max_column]] > 0, 1, -1)  # 0列目と最終列

            # 探索
            mask = mask_process(mask, max_row, max_column)

            # 領域ポイント集計
            my_tile = np.where(mask == 0)
            my_area_score = [abs(self.score_field[i][j]) for i, j in zip(my_tile[0], my_tile[1])]
            my_area_score = np.sum(my_area_score)
            # 合計スコア
            self.my_score = my_tile_score + my_area_score

            return int(self.my_score)

        # 敵チームのスコア
        elif p==-1:
            # タイルポイントの計算
            mask = np.where(self.field < 0)
            position_list = [(i, j) for i, j in zip(mask[0], mask[1])]
            enemy_tile_score = [self.score_field[i][j] for i, j in position_list]
            enemy_tile_score = np.sum(enemy_tile_score)

            # 領域ポイントの計算
            # 敵チームのマスを1, それ以外を0とする
            mask = np.where(self.field < 0, 1, 0)
            # 4辺の敵チーム以外のタイルを-1とする
            mask[[0, max_row], :] = np.where(self.field[[0, max_row], :] < 0, 1, -1)  # 0行目と最終行
            mask[:, [0, max_column]] = np.where(self.field[:, [0, max_column]] < 0, 1, -1)  # 0列目と最終列

            # 探索
            mask = mask_process(mask, max_row, max_column)

            # 領域ポイント集計
            enemy_tile = np.where(mask == 0)
            enemy_area_score = [abs(self.score_field[i][j]) for i, j in zip(enemy_tile[0], enemy_tile[1])]
            enemy_area_score = np.sum(enemy_area_score)
            # 合計スコア
            self.enemy_score = enemy_tile_score + enemy_area_score

            return int(self.enemy_score)

        # 空白マスの合計スコア
        else:
            mask = np.where(self.field == 0)
            position_list = [(i, j) for i, j in zip(mask[0], mask[1])]
            empty_tile_score = [self.score_field[i][j] for i, j in position_list]
            empty_tile_score = np.sum(empty_tile_score)

            # 領域ポイントの計算
            # 敵チームのマスを1, それ以外を0とする
            mask = np.where(self.field < 0, 1, 0)
            # 4辺の敵チーム以外のタイルを-1とする
            mask[[0, max_row], :] = np.where(self.field[[0, max_row], :] < 0, 1, -1)  # 0行目と最終行
            mask[:, [0, max_column]] = np.where(self.field[:, [0, max_column]] < 0, 1, -1)  # 0列目と最終列

            # 探索
            mask = mask_process(mask, max_row, max_column)

            # 領域ポイント集計
            empty_tile = np.where(mask == 0)
            empty_area_score = [abs(self.score_field[i][j]) for i, j in zip(empty_tile[0], empty_tile[1])]
            empty_area_score = np.sum(empty_area_score)
            # 合計スコア
            self.enemy_score = empty_tile_score + empty_area_score

            return int(self.enemy_score)