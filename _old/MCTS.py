import numpy as np
import logging

# logging の設定
logger = logging.getLogger(__name__)
# ログレベルの設定
logger.setLevel(level=logging.WARNING)

# ログのファイル出力先を設定
fh = logging.FileHandler('logs/MCTS.log')
logger.addHandler(fh)

# ログのコンソール出力の設定
sh = logging.StreamHandler()
logger.addHandler(sh)

# ログの出力形式の設定
formatter = logging.Formatter('%(asctime)s:%(lineno)d:%(levelname)s:%(message)s')
fh.setFormatter(formatter)
sh.setFormatter(formatter)


class State:
    pass


class Node:
    pass
