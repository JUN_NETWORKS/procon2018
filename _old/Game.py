"""ゲームプログラム"""

from GameLogic.GameLogic import Board
from GameLogic.GenerateField import generate_field
import random
import numpy as np


class Game:

    def __init__(self):
        pass

    def newGame(self, score_field=None, turns=None, me_1=None, me_2=None):
        """
        引数のデフォルトを None にしているのは実際の試合でも使えるようにするため。
        :param score_field: スコア配置(指定がない場合ランダム)
        :param turns: ターン数(指定がない場合60~120からランダム)
        :param me_1: 初期座標(指定がない場合ランダム)
        :param me_2: 初期座標(指定がない場合ランダム)
        :return:  初期盤面と各エージェントの初期座標を返す
        """

        # score_field が無ければ生成する
        if score_field is None:
            row = np.random.randint(7, 13)  # 行を決める
            min_col = 80 // row + 1
            column = np.random.randint(min_col, 13)  # 80マス以上になるように列を決める

            v_symmetry = bool(random.getrandbits(1))  # 縦対称
            h_symmetry = bool(random.getrandbits(1))  # 横対称

            score_field = generate_field(height=row, width=column, v_symmetry=v_symmetry, h_symmetry=h_symmetry)

        # 初期座標を決める
        me_1_row = np.random.randint(0, len(score_field[:]) // 2 - 1) if me_1 is None else me_1[0]
        me_1_col = np.random.randint(0, len(score_field[0]) // 2 - 1) if me_1 is None else me_1[1]
        me_2_row = np.random.randint(len(score_field[:]) // 2 - 1, len(score_field[:]) - 1) if me_2 is None else me_2[0]
        me_2_col = np.random.randint(len(score_field[0]) // 2 - 1, len(score_field[0]) - 1) if me_2 is None else me_2[1]

        self.b = Board(score_field=score_field, me_1=(me_1_row, me_1_col), me_2=(me_2_row, me_2_col))

        # ターン数
        self.remaining_turn = np.random.randint(60, 121) if turns is None else turns

        # 現在の盤面と現在座標を返す
        return self.b.field, self.remaining_turn, self.b.me_1, self.b.me_2, self.b.enemy_1, self.b.enemy_2

    def takeAction(self, commands={}):
        self.b.action(commands=commands)

        self.remaining_turn -= 1

        return self.b.field, self.remaining_turn, self.b.me_1, self.b.me_2, self.b.enemy_1, self.b.enemy_2

    def result(self):
        return self.b.result()


if __name__ == "__main__":

    # 人間によるデバッグ用
    from GameLogic.human_input import human_input

    env = Game()
    for i in range(2):

        env.newGame(turns=10)
        while env.remaining_turn != 0:
            print(env.b.score_field)
            print()
            print(env.b.field)
            commands = human_input(env.b.field, env.b.me_1, env.b.me_2, env.b.enemy_1, env.b.enemy_2)  # for human
            print(commands)
            field, remaining_turn, me_1, me_2, enemy_1, enemy_2 = env.takeAction(commands=commands)
        winner = env.result()

        print(winner)
        print("Winner: {}".format("me" if winner == 1 else "enemy" if winner == -1 else "draw"))
