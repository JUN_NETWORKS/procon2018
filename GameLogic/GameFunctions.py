import numpy as np
from numba.decorators import jit  # 高速化ライブラリ


# 敵の初期座標を自分の初期座標を元に作成
def generate_enemy_position(field_size=(8, 11), me_1=(1, 1), me_2=(6, 9)):
    enemy_1 = (me_1[0], field_size[1] - me_1[1] - 1)
    enemy_2 = (me_2[0], field_size[1] - me_2[1] - 1)

    return enemy_1, enemy_2


# tuple同士の足し算. 座標計算で使う
def plus_tuple(tuple1, tuple2):
    return (tuple1[0] + tuple2[0], tuple1[1] + tuple2[1])


# 移動 -> 行動後の座標を返す
# 削除 -> エージェント自体は動かないので現在と同じ座標を返す
def move_or_delete(command, current, apply):
    if command == 1:
        return plus_tuple(current, apply)
    else:
        return current


# 領域ポイント計算の探索処理
@jit
def mask_process(mask, max_row, max_column):
    # record0 == record1 になる(領域の場所の確定)までループ
    record0, record1 = 0, 1
    equal = 0  # 前回と同じなら +1 する
    while equal < 10:
        record0 = record1
        for i in range(1, max_row):
            for j in range(1, max_column):

                around = []  # ある座標の周り(上下左右)の値を入れるリスト
                if mask[i][j] == 0:
                    around.append(mask[i + 1][j])
                    around.append(mask[i - 1][j])
                    around.append(mask[i][j + 1])
                    around.append(mask[i][j - 1])

                    if -1 in around:  # -1が周りにあった場合
                        mask[i][j] = -1
                    else:
                        pass
                else:  # 空白以外は無視
                    continue
        record1 = np.sum(mask == 0)
        if record0 == record1:
            equal += 1
        else:
            equal = 0
    return mask