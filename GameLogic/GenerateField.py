﻿"""
フィールドの自動生成
"""

import numpy as np
import math
import random


# 0以下の数の出現率が10%の乱数生成器
def generate_point():
    weight = np.ones(33)
    weight[:17] = 1 / 17 * 0.1  # 0点以下の点数出現率10%を0以下の数字に均等に振る
    weight[17:] = 1 / 16 * 0.9  # 1点以上の点数出現率90%を1以上の数字に均等に振る

    return np.random.choice(range(-16, 17), p=weight)


def generate_field(height, width, v_symmetry, h_symmetry):
    """
    :param height: 縦
    :param width: 横
    :param v_symmetry: 縦対称
    :param h_symmetry: 横対称
    :return: 生成したフィールドの array
    """
    if width * height > 12 * 12:
        print("width*height > 12*12")
    data = np.ones([height, width], dtype=np.int)
    for i_width in range(math.floor(width / 2)):
        for i_height in range(math.floor(height / 2)):
            rand_value = generate_point()
            data[i_height][i_width] = rand_value

            # 横のみ対称
            if (h_symmetry == True and v_symmetry == False):
                data[i_height][width - i_width - 1] = rand_value
                rand_value = random.randint(-10, 10)
                data[height - i_height - 1][i_width] = rand_value
                data[height - i_height - 1][width - i_width - 1] = rand_value

            # 縦のみ対称
            if (h_symmetry == False and v_symmetry == True):
                data[height - i_height - 1][i_width] = rand_value
                rand_value = random.randint(-10, 10)
                data[i_height][width - i_width - 1] = rand_value
                data[height - i_height - 1][width - i_width - 1] = rand_value

            # 縦横の両方が対称
            if (h_symmetry == True and v_symmetry == True):
                data[height - i_height - 1][i_width] = rand_value
                data[i_height][width - i_width - 1] = rand_value
                data[height - i_height - 1][width - i_width - 1] = rand_value

        # 縦が奇数
        if (math.ceil(height / 2) - math.floor(height / 2) > 0):
            data[math.floor(height / 2)][i_width] = random.randint(-10, 10)

    # 横が奇数
    # print(" "+str(math.ceil(width/2))+" "+str(math.floor(width/2)))
    if (math.ceil(width / 2) - math.floor(width / 2) > 0):
        for i_height in range(math.floor(height / 2)):
            rand_value = generate_point()
            data[i_height][math.floor(width / 2)] = rand_value
            if (h_symmetry == True):
                data[height - i_height - 1][math.floor(width / 2)] = rand_value
            else:
                data[height - i_height - 1][math.floor(width / 2)] = random.randint(-10, 10)
        if (math.ceil(height / 2) - math.floor(height / 2) > 0):
            data[math.floor(height / 2)][math.floor(width / 2)] = random.randint(-10, 10)

    # return: field_array
    return data


if __name__ == "__main__":
    width = 9
    height = 9
    v_symmetry = False
    h_symmetry = True
    field = generate_field(height, width, v_symmetry, h_symmetry)

    print(field.shape)
    print(field)
