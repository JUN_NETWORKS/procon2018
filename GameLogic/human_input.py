"""標準入力によるプレイ(デバッグ)するための関数"""
from GameLogic.GameFunctions import *

# 人間がテストする用
def human_input(field, me_1, me_2, enemy_1, enemy_2):
    """
    :param me_1: current position for me_1
    :param me_2: current position for me_2
    :param enemy_1: current position for enemy_1
    :param enemy_2: current position for enemy_2
    :return: commands
    """
    commands = {"me_1": {"command": 1, "direction": (0, 0)},
                "me_2": {"command": 1, "direction": (0, 0)},
                "enemy_1": {"command": 1, "direction": (0, 0)},
                "enemy_2": {"command": 1, "direction": (0, 0)}, }

    # input
    for jj in range(100):  # 入力エラー検出&再入力受付用
        try:
            # 指示パート
            print("エージェント 1 の行動を選択してください")
            commands["me_1"]["command"] = int(input("移動: 1, タイル除去: 2 : "))

            # コマンドが 1,2 以外だった場合
            if commands["me_1"]["command"] != 1 and commands["me_1"]["command"] != 2:
                print("存在しないコマンド番号です")
                continue
            input_position = tuple(map(int, input("方向を(x,y)で指定し、コンマ区切りで入力してください: ").split(",")))
            commands["me_1"]["direction"] = (input_position[1] * -1, input_position[0])  # (x,y) => (row, columns)

            # 移動or除去先の座標にマイナス、もしくはフィールドの縦横サイズ以上が含まれているのはありえない(フィールド範囲外)のでやり直させる
            apply_position = plus_tuple(me_1, commands["me_1"]["direction"])
            if apply_position[0] < 0 or apply_position[1] < 0 or apply_position[0] > field.shape[0] - 1 or \
                    apply_position[1] > field.shape[1] - 1:
                print("\n\n行動先座標に存在しない座標が含まれています\n\n")
                continue

            # 移動先が敵マスの場合移動でgit fetch origin masterきないのでエラーを返す
            if commands["me_1"]["command"] == 1 and field[apply_position[0]][apply_position[1]] < 0:
                print("移動先のマスは相手のマスなので移動できません")
                continue

        except ValueError:
            print("\n\n入力エラー。もう一度入力しなおしてください\n")
            continue
        except IndexError:
            print("\n\nインデックスエラー。もう一度入力しなおしてください\n")
            continue
        break

    for jj in range(100):  # 入力エラー検出&再入力受付用
        try:
            print("エージェント 2 の行動を選択してください")
            commands["me_2"]["command"] = int(input("移動: 1, タイル除去: 2 : "))
            # コマンドが 1,2 以外だった場合
            if commands["me_2"]["command"] != 1 and commands["me_2"]["command"] != 2:
                print("存在しないコマンド番号です")
                continue
            input_position = tuple(map(int, input("方向を(x,y)で指定し、コンマ区切りで入力してください: ").split(",")))
            commands["me_2"]["direction"] = (input_position[1] * -1, input_position[0])
            # 移動or除去先の座標にマイナスが含まれているのはありえない(フィールド範囲外)のでやり直させる
            apply_position = plus_tuple(me_2, commands["me_2"]["direction"])
            if apply_position[0] < 0 or apply_position[1] < 0 or apply_position[0] > field.shape[0] - 1 or \
                    apply_position[1] > field.shape[1] - 1:
                print("\n\n行動先座標に存在しない座標が含まれています\n\n")
                continue
            # 移動先が敵マスの場合移動できないのでエラーを返す
            if commands["me_2"]["command"] == 1 and field[apply_position[0]][apply_position[1]] < 0:
                print("移動先のマスは相手のマスなので移動できません")
                continue
        except ValueError:
            print("\n\n入力エラー。もう一度入力しなおしてください\n")
            continue
        except IndexError:
            print("\n\nインデックスエラー。もう一度入力しなおしてください\n")
            continue
        break

    print("\n\n敵の指示。\n")

    for jj in range(100):  # 入力エラー検出&再入力受付用
        try:
            print("エージェント -1 の行動を選択してください")
            commands["enemy_1"]["command"] = int(input("移動: 1, タイル除去: 2 : "))
            # コマンドが 1,2 以外だった場合
            if commands["me_1"]["command"] != 1 and commands["me_1"]["command"] != 2:
                print("存在しないコマンド番号です")
                continue
            input_position = tuple(map(int, input("方向を(x,y)で指定し、コンマ区切りで入力してください: ").split(",")))
            commands["enemy_1"]["direction"] = (input_position[1] * -1, input_position[0])
            # 移動or除去先の座標にマイナスが含まれているのはありえない(フィールド範囲外)のでやり直させる
            apply_position = plus_tuple(enemy_1, commands["enemy_1"]["direction"])
            if apply_position[0] < 0 or apply_position[1] < 0:
                print("\n\n行動先座標に存在しない座標が含まれています\n\n")
                continue
            # 移動先が敵マスの場合移動できないのでエラーを返す
            if commands["enemy_1"]["command"] == 1 and field[apply_position[0]][apply_position[1]] > 0:
                print("移動先のマスは相手のマスなので移動できません")
                continue
        except ValueError:
            print("\n\n入力エラー。もう一度入力しなおしてください\n")
            continue
        except IndexError:
            print("\n\nインデックスエラー。もう一度入力しなおしてください\n")
            continue
        break

    for jj in range(100):  # 入力エラー検出&再入力受付用
        try:
            print("エージェント -2 の行動を選択してください")
            commands["enemy_2"]["command"] = int(input("移動: 1, タイル除去: 2 : "))
            # コマンドが 1,2 以外だった場合
            if commands["me_1"]["command"] != 1 and commands["me_1"]["command"] != 2:
                print("存在しないコマンド番号です")
                continue
            input_position = tuple(map(int, input("方向を(x,y)で指定し、コンマ区切りで入力してください: ").split(",")))
            commands["enemy_2"]["direction"] = (input_position[1] * -1, input_position[0])
            # 移動or除去先の座標にマイナスが含まれているのはありえない(フィールド範囲外)のでやり直させる
            apply_position = plus_tuple(enemy_2, commands["enemy_2"]["direction"])
            if apply_position[0] < 0 or apply_position[1] < 0:
                print("\n\n行動先座標に存在しない座標が含まれています\n\n")
                continue
            # 移動先が敵マスの場合移動できないのでエラーを返す
            if commands["enemy_2"]["command"] == 1 and field[apply_position[0]][apply_position[1]] > 0:
                print("移動先のマスは相手のマスなので移動できません")
                continue
        except ValueError:
            print("\n\n入力エラー。もう一度入力しなおしてください\n")
            continue
        except IndexError:
            print("\n\nインデックスエラー。もう一度入力しなおしてください\n")
            continue
        break

    return commands