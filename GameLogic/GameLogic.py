"""Board クラス"""
import numpy as np
from numba.decorators import jit  # 高速化ライブラリ


# 敵の初期座標を自分の初期座標を元に作成
def generate_enemy_position(field_size=(8, 11), me_1=(1, 1), me_2=(6, 9)):
    enemy_1 = (me_1[0], field_size[1] - me_1[1] - 1)
    enemy_2 = (me_2[0], field_size[1] - me_2[1] - 1)

    return enemy_1, enemy_2


# tuple同士の足し算. 座標計算で使う
def plus_tuple(tuple1, tuple2):
    return tuple(x + y for x, y in zip(tuple1, tuple2))


# 移動 -> 行動後の座標を返す
# 削除 -> エージェント自体は動かないので現在と同じ座標を返す
def move_or_delete(command, current, apply):
    if command == 1:
        return plus_tuple(current, apply)
    else:
        return current


# 領域ポイント計算の探索処理
@jit
def mask_process(mask, max_row, max_column):
    # record0 == record1 になる(領域の場所の確定)までループ
    record0, record1 = 0, 1
    equal = 0  # 前回と同じなら +1 する
    while equal < 10:
        record0 = record1
        for i in range(1, max_row):
            for j in range(1, max_column):

                around = []  # ある座標の周り(上下左右)の値を入れるリスト
                if mask[i][j] == 0:
                    around.append(mask[i + 1][j])
                    around.append(mask[i - 1][j])
                    around.append(mask[i][j + 1])
                    around.append(mask[i][j - 1])

                    if -1 in around:  # -1が周りにあった場合
                        mask[i][j] = -1
                    else:
                        pass
                else:  # 空白以外は無視
                    continue
        record1 = np.sum(mask == 0)
        if record0 == record1:
            equal += 1
        else:
            equal = 0
    return mask


class Board:

    def __init__(self, score_field, me_1=(1, 1), me_2=(6, 9)):
        # Setup Board

        # 各マスの点数が入ってる
        self.score_field = score_field
        # どっちのチームがどのマスを持っているかの情報 1: me_1, 2: me_2, -1: enemy_1, -2: enemy_2, 0: none
        self.field = np.zeros(score_field.shape, dtype=int)

        # my team position
        self.me_1 = me_1
        self.me_2 = me_2
        # Genetate enemy position
        self.enemy_1, self.enemy_2 = generate_enemy_position(score_field.shape, me_1, me_2)

        # plot init position
        self.field[self.me_1[0]][self.me_1[1]] = 1
        self.field[self.me_2[0]][self.me_2[1]] = 2
        self.field[self.enemy_1[0]][self.enemy_1[1]] = -1
        self.field[self.enemy_2[0]][self.enemy_2[1]] = -2

    # 番号に応じたアージェントを返す
    def select_player(self, player_number):
        players = {"1": self.me_1, "2": self.me_2, "-1": self.enemy_1, "-2": self.enemy_2}
        return players[str(player_number)]

    # 移動
    def move(self, agent_num=1, direction=(-1, 1)):
        agent = self.select_player(agent_num)
        agent_position = plus_tuple(agent, direction)
        self.field[agent_position[0]][agent_position[1]] = agent_num  # 移動先のタイルを支配する

        # 移動後の座標を代入する
        agent = self.select_player(agent_num)
        agent = agent_position
        # if agent_num == 1:
        #     self.me_1 = agent_position
        # elif agent_num == 2:
        #     self.me_2 = agent_position
        # elif agent_num == -1:
        #     self.enemy_1 = agent_position
        # elif agent_num == -2:
        #     self.enemy_2 = agent_position
        # else:
        #     print("Error")

    # タイル除去
    def delete_tile(self, player_number=1, direction=(-1, 1)):
        player = self.select_player(player_number)
        tile = plus_tuple(player, direction)
        self.field[tile[0]][tile[1]] = 0

    # コマンドナンバーで行動を分岐する関数
    def branch_action(self, player_num=1, action_command=1, direction=(-1, 1)):
        if action_command == 1:
            self.move(agent_num=player_num, direction=direction)
        else:
            self.delete_tile(player_number=player_num, direction=direction)

    # 実際に行動する際に使う関数
    def action(self, commands):
        """
        :argument :commands {"me_1": {"command": 1, direction: (x,y)}...}
        """
        # Agent list  AgentName: AgentNumber
        agents = {"me_1": 1, "me_2": 2, "enemy_1": -1, "enemy_2": -2}

        # 整合性チェック
        for agent in agents.keys():

            # (x,y) => (row, column)
            commands[agent]["direction"] = (commands[agent]["direction"][1] * -1, commands[agent]["direction"][0])

            # 移動or除去先の座標にマイナス、もしくはフィールドの縦横サイズ以上が含まれているのはルール違反なので移動をしないようにする
            apply_position = plus_tuple(self.me_1, commands[agent]["direction"])
            if apply_position[0] < 0 or apply_position[1] < 0 or apply_position[0] > self.field.shape[0] - 1 or \
                    apply_position[1] > self.field.shape[1] - 1:
                commands[agent]["direction"] = (0, 0)

            # 移動先が敵マスの場合移動できないので移動しないようにする
            if agent == "me_1" or agent == "me_2":
                if commands[agent]["command"] == 1 and self.field[apply_position[0]][apply_position[1]] < 0:
                    commands[agent]["direction"] = (0, 0)
            elif agent == "enemy_1" or agent == "enemy_2":
                if commands[agent]["command"] == 1 and self.field[apply_position[0]][apply_position[1]] > 0:
                    commands[agent]["direction"] = (0, 0)

        # 行動パート
        # 現在の座標を記録しておく
        me_1_position, me_2_position, ene_1_position, ene_2_position = self.me_1, self.me_2, self.enemy_1, self.enemy_2
        now_positions = [me_1_position, me_2_position, ene_1_position, ene_2_position]
        # player: エージェント番号, command: 各エージェントの入力情報, del_num: エージェントの座標情報被り部分の検出処理用, current_position: 現在座標
        for agent, command, del_num, current_position in zip(agents, commands.items(), [0, 1, -2, -1],
                                                             now_positions):
            # 各エージェントで対象マスのかぶりが無いかチェックする
            directions = [plus_tuple(current_direction, commands[agent]["direction"])
                          for agent, current_direction in
                          zip(commands, now_positions)]  # 全エージェントの行動後の座標リスト

            # ループ処理対象のエージェントの座標情報を消す
            del directions[del_num]

            # ループ処理チームエージェントの行動先が別チームのエージェントと被った場合は行動しない
            if plus_tuple(current_position, command[1]["direction"]) in directions:
                print("\nエージェント {} は相手と行動先が被ったため無効となります。\n".format(agent))
                continue

            self.branch_action(player_num=agent, action_command=command[1]["command"],
                               direction=command[1]["direction"])

        # 今の盤面を返す
        return self.field

    # 勝敗を返す
    def result(self):
        # インデックスの最大行と最大列
        max_row = self.field.shape[0] - 1
        max_column = self.field.shape[1] - 1

        # 自チーム
        # タイルポイントの計算
        mask = np.where(self.field > 0)
        position_list = [(i, j) for i, j in zip(mask[0], mask[1])]
        my_tile_score = [self.score_field[i][j] for i, j in position_list]
        my_tile_score = np.sum(my_tile_score)

        # 領域ポイントの計算
        # 自チームのマスを1, それ以外を0とする
        mask = np.where(self.field > 0, 1, 0)
        # 4辺の自チーム以外のタイルを-1とする
        mask[[0, max_row], :] = np.where(self.field[[0, max_row], :] > 0, 1, -1)  # 0行目と最終行
        mask[:, [0, max_column]] = np.where(self.field[:, [0, max_column]] > 0, 1, -1)  # 0列目と最終列

        # 探索(movable[0][0],movable[1][0],movable[2][0])
        mask = mask_process(mask, max_row, max_column)

        # 領域ポイント集計
        my_tile = np.where(mask == 0)
        my_area_score = [abs(self.score_field[i][j]) for i, j in zip(my_tile[0], my_tile[1])]
        my_area_score = np.sum(my_area_score)
        # 合計スコア
        self.my_score = my_tile_score + my_area_score

        # 敵チームのスコア
        # タイルポイントの計算
        mask = np.where(self.field < 0)
        position_list = [(i, j) for i, j in zip(mask[0], mask[1])]
        enemy_tile_score = [self.score_field[i][j] for i, j in position_list]
        enemy_tile_score = np.sum(enemy_tile_score)

        # 領域ポイントの計算
        # 敵チームのマスを1, それ以外を0とする
        mask = np.where(self.field < 0, 1, 0)
        # 4辺の敵チーム以外のタイルを-1とする
        mask[[0, max_row], :] = np.where(self.field[[0, max_row], :] < 0, 1, -1)  # 0行目と最終行
        mask[:, [0, max_column]] = np.where(self.field[:, [0, max_column]] < 0, 1, -1)  # 0列目と最終列

        # 探索
        mask = mask_process(mask, max_row, max_column)

        # 領域ポイント集計
        enemy_tile = np.where(mask == 0)
        enemy_area_score = [abs(self.score_field[i][j]) for i, j in zip(enemy_tile[0], enemy_tile[1])]
        enemy_area_score = np.sum(enemy_area_score)
        # 合計スコア
        self.enemy_score = enemy_tile_score + enemy_area_score

        # デバッグ用
        print("自チーム\nタイルポイント{}\n領域ポイント{}".format(my_tile_score, my_area_score))
        print("敵チーム\nタイルポイント{}\n領域ポイント{}".format(enemy_tile_score, enemy_area_score))

        # ターン数が終了ターン数になった場合勝敗を返す

        if self.my_score > self.enemy_score:
            return 1
        elif self.my_score < self.enemy_score:
            return -1
        else:
            return 0
