"""
人間vs人
"""
import numpy as np
from GameFunctions import *


class Board():

    def __init__(self, score_field=np.random.randint(-2, 16, size=(8, 11)), me_1=(1, 1), me_2=(6, 9)):
        # Setup Board

        # 各マスの点数が入ってる
        self.score_field = score_field
        # どっちのチームがどのマスを持っているかの情報 1: me_1, 2: me_2, -1: enemy_1, -2: enemy_2, 0: none
        self.field = np.zeros(score_field.shape, dtype=int)

        # my team position
        self.me_1 = me_1
        self.me_2 = me_2
        # Genetate enemy position
        self.enemy_1, self.enemy_2 = generate_enemy_position(score_field.shape, me_1, me_2)

        # plot init position
        self.field[self.me_1[0]][self.me_1[1]] = 1
        self.field[self.me_2[0]][self.me_2[1]] = 2
        self.field[self.enemy_1[0]][self.enemy_1[1]] = -1
        self.field[self.enemy_2[0]][self.enemy_2[1]] = -2

    def select_player(self, player_number):
        players = {"1": self.me_1, "2": self.me_2, "-1": self.enemy_1, "-2": self.enemy_2}
        return players[str(player_number)]

    # 移動
    def move(self, player_number=1, direction=(-1, 1)):
        player = self.select_player(player_number)
        player = plus_tuple(player, direction)
        self.field[player[0]][player[1]] = player_number  # 移動先のタイルを支配する

        # 移動後の座標を代入する
        if player_number == 1:
            self.me_1 = player
        elif player_number == 2:
            self.me_2 = player
        elif player_number == -1:
            self.enemy_1 = player
        elif player_number == -2:
            self.enemy_2 = player
        else:
            print("Error")

    # タイル除去
    def delete_tile(self, player_number=1, direction=(-1, 1)):
        player = self.select_player(player_number)
        tile = plus_tuple(player, direction)
        self.field[tile[0]][tile[1]] = 0

    # 実際に使用する関数
    def action(self, player_num=1, action_command=1, direction=(-1, 1)):
        if action_command == 1:
            self.move(player_number=player_num, direction=direction)
        else:
            self.delete_tile(player_number=player_num, direction=direction)

    # 結果表示
    def result(self):
        # インデックスの最大行と最大列
        max_row = self.field.shape[0] - 1
        max_column = self.field.shape[1] - 1

        # 自チーム
        # タイルポイントの計算
        mask = np.where(self.field > 0)
        position_list = [(i, j) for i, j in zip(mask[0], mask[1])]
        my_tile_score = [self.score_field[i][j] for i, j in position_list]
        my_tile_score = np.sum(my_tile_score)

        # 領域ポイントの計算
        # 自チームのマスを1, それ以外を0とする
        mask = np.where(self.field > 0, 1, 0)
        # 4辺の自チーム以外のタイルを-1とする
        mask[[0, max_row], :] = np.where(self.field[[0, max_row], :] > 0, 1, -1)  # 0行目と最終行
        mask[:, [0, max_column]] = np.where(self.field[:, [0, max_column]] > 0, 1, -1)  # 0列目と最終列

        # 探索
        mask = mask_process(mask, max_row, max_column)

        # 領域ポイント集計
        my_tile = np.where(mask == 0)
        my_area_score = [abs(self.score_field[i][j]) for i, j in zip(my_tile[0], my_tile[1])]
        my_area_score = np.sum(my_area_score)
        # 合計スコア
        self.my_score = my_tile_score + my_area_score

        # 敵チームのスコア
        # タイルポイントの計算
        mask = np.where(self.field < 0)
        position_list = [(i, j) for i, j in zip(mask[0], mask[1])]
        enemy_tile_score = [self.score_field[i][j] for i, j in position_list]
        enemy_tile_score = np.sum(enemy_tile_score)

        # 領域ポイントの計算
        # 敵チームのマスを1, それ以外を0とする
        mask = np.where(self.field < 0, 1, 0)
        # 4辺の敵チーム以外のタイルを-1とする
        mask[[0, max_row], :] = np.where(self.field[[0, max_row], :] < 0, 1, -1)  # 0行目と最終行
        mask[:, [0, max_column]] = np.where(self.field[:, [0, max_column]] < 0, 1, -1)  # 0列目と最終列

        # 探索
        mask = mask_process(mask, max_row, max_column)

        # 領域ポイント集計
        enemy_tile = np.where(mask == 0)
        enemy_area_score = [abs(self.score_field[i][j]) for i, j in zip(enemy_tile[0], enemy_tile[1])]
        enemy_area_score = np.sum(enemy_area_score)
        # 合計スコア
        self.enemy_score = enemy_tile_score + enemy_area_score

        # デバッグ用
        print("自チーム\nタイルポイント{}\n領域ポイント{}".format(my_tile_score, my_area_score))
        print("敵チーム\nタイルポイント{}\n領域ポイント{}".format(enemy_tile_score, enemy_area_score))

        return int(self.my_score), int(self.enemy_score)


if __name__ == "__main__":

    # Procon2018 資料にあった点数配置
    score_field = np.array([
        [-2, 1, 0, 1, 2, 0, 2, 1, 0, 1, -2],
        [1, 3, 2, -2, 0, 1, 0, -2, 2, 3, 1],
        [1, 3, 2, 1, 0, -2, 0, 1, 2, 3, 1],
        [2, 1, 1, 2, 2, 3, 2, 2, 1, 1, 2],
        [2, 1, 1, 2, 2, 3, 2, 2, 1, 1, 2],
        [1, 3, 2, 1, 0, -2, 0, 1, 2, 3, 1],
        [1, 3, 2, -2, 0, 1, 0, -2, 2, 3, 1],
        [-2, 1, 0, 1, 2, 0, 2, 1, 0, 1, -2]
    ])

    b = Board(score_field=score_field)

    # turns = np.random.randint(60,120)
    turns = 10
    print("ターン数: {}".format(turns))
    print("あなたのエージェントは 1 と 2 です。")
    print("敵のエージェントは -1 と -2 です。")
    print("0 は何も置かれていなマスです。")
    print("最大行番号: {}\n最大列番号: {}".format(b.field.shape[0] - 1, b.field.shape[1] - 1))

    print("\n\nゲーム開始")
    print("------------------------------\n")

    commands = {"me_1": {"command": 1, "direction": (-1, 1)}, \
                "me_2": {"command": 1, "direction": (-1, 1)}, \
                "enemy_1": {"command": 1, "direction": (-1, 1)}, \
                "enemy_2": {"command": 1, "direction": (-1, 1)}}

    for turn in range(turns):
        print("\n\n")
        print("ターン{}\n".format(turn))
        print("各マスの点数\n{}\n".format(b.score_field))
        print("現在のマスの占拠状況\n{}\n".format(b.field))
        print("現在の特典")
        print("あなたのエージェントの現在地座標(row, columns)\nエージェント1: {}\nエージェント2: {}\n".format(b.me_1, b.me_2))
        print("敵のエージェントの現在地座標(row, columns)\nエージェント-1: {}\nエージェント-2: {}\n".format(b.enemy_1, b.enemy_2))

        # 指示パート
        print("あなたの指示。\n")

        for jj in range(100):  # 入力エラー検出&再入力受付用
            try:
                # 指示パート
                print("エージェント 1 の行動を選択してください")
                commands["me_1"]["command"] = int(input("移動: 1, タイル除去: 2 : "))

                # コマンドが 1,2 以外だった場合
                if commands["me_1"]["command"] != 1 and commands["me_1"]["command"] != 2:
                    print("存在しないコマンド番号です")
                    continue
                input_position = tuple(map(int, input("方向を(x,y)で指定し、コンマ区切りで入力してください: ").split(",")))
                commands["me_1"]["direction"] = (input_position[1] * -1, input_position[0])  # (x,y) => (row, columns)

                # 移動or除去先の座標にマイナス、もしくはフィールドの縦横サイズ以上が含まれているのはありえない(フィールド範囲外)のでやり直させる
                apply_position = plus_tuple(b.me_1, commands["me_1"]["direction"])
                if apply_position[0] < 0 or apply_position[1] < 0 or apply_position[0] > b.field.shape[0] - 1 or \
                        apply_position[1] > b.field.shape[1] - 1:
                    print("\n\n行動先座標に存在しない座標が含まれています\n\n")
                    continue

                # 移動先が敵マスの場合移動できないのでエラーを返す
                if commands["me_1"]["command"] == 1 and b.field[apply_position[0]][apply_position[1]] < 0:
                    print("移動先のマスは相手のマスなので移動できません")
                    continue

            except ValueError:
                print("\n\n入力エラー。もう一度入力しなおしてください\n")
                continue
            except IndexError:
                print("\n\nインデックスエラー。もう一度入力しなおしてください\n")
                continue
            break

        for jj in range(100):  # 入力エラー検出&再入力受付用
            try:
                print("エージェント 2 の行動を選択してください")
                commands["me_2"]["command"] = int(input("移動: 1, タイル除去: 2 : "))
                # コマンドが 1,2 以外だった場合
                if commands["me_2"]["command"] != 1 and commands["me_2"]["command"] != 2:
                    print("存在しないコマンド番号です")
                    continue
                input_position = tuple(map(int, input("方向を(x,y)で指定し、コンマ区切りで入力してください: ").split(",")))
                commands["me_2"]["direction"] = (input_position[1] * -1, input_position[0])
                # 移動or除去先の座標にマイナスが含まれているのはありえない(フィールド範囲外)のでやり直させる
                apply_position = plus_tuple(b.me_2, commands["me_2"]["direction"])
                if apply_position[0] < 0 or apply_position[1] < 0 or apply_position[0] > b.field.shape[0] - 1 or \
                        apply_position[1] > b.field.shape[1] - 1:
                    print("\n\n行動先座標に存在しない座標が含まれています\n\n")
                    continue
                # 移動先が敵マスの場合移動できないのでエラーを返す
                if commands["me_2"]["command"] == 1 and b.field[apply_position[0]][apply_position[1]] < 0:
                    print("移動先のマスは相手のマスなので移動できません")
                    continue
            except ValueError:
                print("\n\n入力エラー。もう一度入力しなおしてください\n")
                continue
            except IndexError:
                print("\n\nインデックスエラー。もう一度入力しなおしてください\n")
                continue
            break

        print("\n\n敵の指示。\n")

        for jj in range(100):  # 入力エラー検出&再入力受付用
            try:
                print("エージェント -1 の行動を選択してください")
                commands["enemy_1"]["command"] = int(input("移動: 1, タイル除去: 2 : "))
                # コマンドが 1,2 以外だった場合
                if commands["me_1"]["command"] != 1 and commands["me_1"]["command"] != 2:
                    print("存在しないコマンド番号です")
                    continue
                input_position = tuple(map(int, input("方向を(x,y)で指定し、コンマ区切りで入力してください: ").split(",")))
                commands["enemy_1"]["direction"] = (input_position[1] * -1, input_position[0])
                # 移動or除去先の座標にマイナスが含まれているのはありえない(フィールド範囲外)のでやり直させる
                apply_position = plus_tuple(b.enemy_1, commands["enemy_1"]["direction"])
                if apply_position[0] < 0 or apply_position[1] < 0:
                    print("\n\n行動先座標に存在しない座標が含まれています\n\n")
                    continue
                # 移動先が敵マスの場合移動できないのでエラーを返す
                if commands["enemy_1"]["command"] == 1 and b.field[apply_position[0]][apply_position[1]] > 0:
                    print("移動先のマスは相手のマスなので移動できません")
                    continue
            except ValueError:
                print("\n\n入力エラー。もう一度入力しなおしてください\n")
                continue
            except IndexError:
                print("\n\nインデックスエラー。もう一度入力しなおしてください\n")
                continue
            break

        for jj in range(100):  # 入力エラー検出&再入力受付用
            try:
                print("エージェント -2 の行動を選択してください")
                commands["enemy_2"]["command"] = int(input("移動: 1, タイル除去: 2 : "))
                # コマンドが 1,2 以外だった場合
                if commands["me_1"]["command"] != 1 and commands["me_1"]["command"] != 2:
                    print("存在しないコマンド番号です")
                    continue
                input_position = tuple(map(int, input("方向を(x,y)で指定し、コンマ区切りで入力してください: ").split(",")))
                commands["enemy_2"]["direction"] = (input_position[1] * -1, input_position[0])
                # 移動or除去先の座標にマイナスが含まれているのはありえない(フィールド範囲外)のでやり直させる
                apply_position = plus_tuple(b.enemy_2, commands["enemy_2"]["direction"])
                if apply_position[0] < 0 or apply_position[1] < 0:
                    print("\n\n行動先座標に存在しない座標が含まれています\n\n")
                    continue
                # 移動先が敵マスの場合移動できないのでエラーを返す
                if commands["enemy_2"]["command"] == 1 and b.field[apply_position[0]][apply_position[1]] > 0:
                    print("移動先のマスは相手のマスなので移動できません")
                    continue
            except ValueError:
                print("\n\n入力エラー。もう一度入力しなおしてください\n")
                continue
            except IndexError:
                print("\n\nインデックスエラー。もう一度入力しなおしてください\n")
                continue
            break

        # 行動パート
        # 現在の座標を記録しておく
        me_1_posi, me_2_posi, ene_1_posi, ene_2_posi = b.me_1, b.me_2, b.enemy_1, b.enemy_2
        now_position = [me_1_posi, me_2_posi, ene_1_posi, ene_2_posi]
        # player: エージェント番号, command: 各エージェントの入力情報, del_num: エージェントの座標情報被り部分の検出処理用, current_position: 現在座標
        for player, command, del_num, current_position in zip([1, 2, -1, -2], commands.items(), [0, 1, -2, -1],
                                                              now_position):
            # 各エージェントで対象マスのかぶりが無いかチェックする
            directions = [plus_tuple(current_direction, commands[agent]["direction"])
                          for agent, current_direction in
                          zip(commands, now_position)]  # 全エージェントの行動後の座標リスト

            # ループ処理対象のエージェントの座標情報を消す
            del directions[del_num]
            # if player == 1 or player == 2:
            #     del directions[0]
            #     del directions[1]
            # else:
            #     del directions[-1]
            #     del directions[-2]

            if plus_tuple(current_position,
                          command[1]["direction"]) in directions:  # ループ処理チームエージェントの行動先が別チームのエージェントと被った場合は行動しない
                print("\nエージェント{}は相手と行動先が被ったため無効となります。\n".format(player))
                continue

            b.action(player_num=player, action_command=command[1]["command"], direction=command[1]["direction"])

        me_score, enemy_score = b.result()
        print("")
        print("自チームのスコア: {}\n敵チームのスコア{}".format(me_score, enemy_score))

    print("\n\n")
    print("ゲーム終了")
    me_score, enemy_score = b.result()
    print("各マスの点数\n{}\n".format(b.score_field))
    print("最終的なマスの占拠状況\n{}\n".format(b.field))
    print("")
    print("自チームのスコア: {}\n敵チームのスコア{}".format(me_score, enemy_score))
    if me_score > enemy_score:
        print("自チームの勝ち！")
    elif me_score < enemy_score:
        print("敵チームの勝ち")
    else:
        print("引き分け！！")
