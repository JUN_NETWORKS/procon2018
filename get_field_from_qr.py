from read_QR import read_QR
import numpy as np


def get_field_from_qr():
    # 読み込みと前処理
    qr_data = read_QR()
    qr_data = qr_data.decode("utf-8")
    qr_data = qr_data.split(":")
    qr_data = [row.split(" ") for row in qr_data]
    qr_data.pop(-1)

    # 空文字を消す & string -> int
    content = []
    for row in qr_data:
        if "" in row:
            row.remove("")
        content.append(list(map(int, row)))

    qr_data = content

    # 各要素を取り出す
    field_shape = qr_data[0]
    score_field = qr_data[1:field_shape[0] + 1]
    me_1 = qr_data[field_shape[0] + 1]
    me_2 = qr_data[field_shape[0] + 2]

    return field_shape, np.array(score_field), me_1, me_2


if __name__ == "__main__":
    field_shape, score_field, me_1, me_2 = get_field_from_qr()

    print(field_shape)
    print(score_field)
    print(me_1)
    print(me_2)
