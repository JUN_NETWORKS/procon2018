import numpy as np
from GameLogic import Board


class Game():
    """hashに使うならパターンが少ない方が良い
    This class specifies the base Game class. To define your own game, subclass
    this class and implement the functions below. This works when the game is
    two-player, adversarial and turn-based.

    Use 1 for player1 and -1 for player2.

    See othello/OthelloGame.py for an example implementation.
    """

    def __init__(self, score_field=None, white_1=None, white_2=None):
        # プロコン本番の時に使う変数
        self.score_field = score_field
        self.white_1 = white_1
        self.white_2 = white_2

        # 意思表示記録する変数
        self.white_a = None
        self.black_a = None

    def getInitBoard(self, turn):
        """
        Returns:
            startBoard: a representation of the board (ideally this is the form
                        that will be the input to your neural network)
        """
        b = Board(self.score_field, self.white_1, self.white_2, turn=turn)
        return b.board

    def getBoardSize(self):
        """
        Returns:
            (row, column, channels): a tuple of board dimensions
        """
        return (12, 12, 9)

    def getActionSize(self):
        """
        Returns:
            actionSize: number of all possible actions
        """
        return 16 * 16

    def getCanonicalForm(self, board, player):
        # player-1 のときはチャンネルの構造を変えて渡す
        board = np.copy(board)
        if player == -1:
            # プレイヤーの占有状況
            white_b = np.copy(board[:, :, 1])
            black_b = np.copy(board[:, :, 2])
            board[:, :, 1] = black_b
            board[:, :, 2] = white_b

            # エージェント
            white_1 = np.copy(board[:, :, 3])
            white_2 = np.copy(board[:, :, 4])
            black_1 = np.copy(board[:, :, 5])
            black_2 = np.copy(board[:, :, 6])
            board[:, :, 3] = black_1
            board[:, :, 4] = black_2
            board[:, :, 5] = white_1
            board[:, :, 6] = white_2

        return board

    def getNextState(self, board, player, action):
        """
        Input:
            player: current player (1 or -1)
            action: action taken by current player
                    (actionNum, (row,column))
                    ex: ((1, (-1, -1)), (1, (-1, -1)))

        Returns:
            nextBoard: board after applying action
            nextPlayer: player who plays in the next turn (should be -player)
        """
        # 関数を呼び出すたびに意思表示(両者揃っていたら行動)
        # それに加え-player を返す(次のプレイヤー番号を返す)

        if player == 1:
            self.white_a = action
        else:
            self.black_a = action

        if self.white_a and self.black_a:
            b = Board(board=np.copy(board))
            # TODO: デバッグ用。 あとで消す
            # print("white ac: {}".format(self.white_a))
            # print("black ac: {}".format(self.black_a))
            b.take_action(1, self.white_a)
            b.take_action(-1, self.black_a)
            # リセット
            self.white_a, self.black_a = None, None

            return b.board, -player

        return board, -player

    def getValidMoves(self, board, player):
        """
        Input:
            board: current board
            player: current player

        Returns:
            validMoves: a binary vector of length self.getActionSize(), 1 for
                        moves that are valid from the current board and player,
                        0 for invalid moves
        """
        # 行動価値のある行動は1,価値のない行動は0を付けた256のarrayを返す
        b = Board(board=np.copy(board))
        return b.get_valid_actions(player)

    def getGameEnded(self, board, player):
        """
        Input:
            board: current board
            player: current player (1 or -1)

        Returns:
            r: 0 if game has not ended. 1 if player won, -1 if player lost,
               small non-zero value for draw.
               ゲームが終わったかどうかを返す。また、ゲームが終わった場合勝敗を返す
        """
        # ゲームが終わったかどうかを返す
        b = Board(board=np.copy(board))

        if board[:, :, 8][0, 0] != 0:
            return 0
        if b.count_diff(player) > 0:
            return 1
        return -1

    def stringRepresentation(self, board):
        """
        Input:
            board: current board

        Returns:
            boardString: a quick conversion of board to a string format.
                         Required by MCTS for hashing.
        """

        board = np.copy(np.copy(board))

        # hashに使うならパターンが少ない方が良い
        # マスの点数は値によってカテゴライズする(ex: 0<x<=8: 1)
        score_field = board[:, :, 0]
        score_field = np.where((8 >= score_field) & (score_field > 0), 1, score_field)
        score_field = np.where((16 >= score_field) & (score_field > 8), 2, score_field)
        score_field = np.where((0 > score_field) & (score_field >= -8), -1, score_field)
        score_field = np.where((-8 > score_field) & (score_field >= -16), -2, score_field)
        board = np.dstack((score_field, board[:, :, 1:]))

        # 残りターン数もグループ分けする
        rest_of_turn = board[:, :, 8][0, 0]
        if rest_of_turn >= 81:
            board[:, :, 8][:] = 4
        elif 80 >= rest_of_turn >= 41:
            board[:, :, 8][:] = 3
        elif 40 >= rest_of_turn >= 21:
            board[:, :, 8][:] = 2
        elif 20 >= rest_of_turn >= 1:
            board[:, :, 8][:] = 1
        else:
            pass

        return board.tostring()

    def getScore(self, board, player):
        b = Board(board=np.copy(board))
        return b.get_score(player)


def display(board):
    white_b = board[:, :, 1]
    black_b = board[:, :, 2]

    for row in range(12):
        print(row, "|", end="")
    print("")
    print(" -----------------------")
    for row in range(12):
        print(row, "|", end="")  # print the row #
        for column in range(12):
            white_piece = white_b[row][column]
            black_piece = black_b[row][column]  # get the piece to print
            if black_piece == 1:
                print("b  ", end="")
            elif white_piece == 1:
                print("W  ", end="")
            else:
                if column == 12:
                    print("-", end="")
                else:
                    print("-  ", end="")
        print("|")

    print("   -----------------------")
