import Arena
from MCTS import MCTS
from Game import Game, display
from GamePlayers import *
from NNet import NNetWrapper as NNet

import numpy as np
from utils import *

"""
use this script to play any two agents against each other, or play manually with
any agent.
"""

# TODO: ゲーム本番用にランダムなフィールドではなく任意のフィールドで対戦できるようにする

# QRコードから文字列を読み込む

# QRから読み込んだ文字列を フィールド、 エージェント1初期座標, エージェント2の初期座標に分ける関数を作って実行する

g = Game()

# all players
# rp = RandomPlayer(g).play
# gp = GreedyPlayer(g).play
# hp = HumanPlayer(g).play

# ここの右辺を変更すればプレイヤーを変更できる
gPlayer = RandomPlayer(g)
gPlayer.player_num = -1
g_player = gPlayer.play

# nnet players
n1 = NNet(g)
n1.load_checkpoint('./pretrained_models/','6x100x25_best.pth.tar')
args1 = dotdict({'numMCTSSims': 50, 'cpuct':1.0})
mcts1 = MCTS(g, n1, args1)
n1p = lambda x: np.argmax(mcts1.getActionProb(x, 1, temp=0))


#n2 = NNet(g)
#n2.load_checkpoint('/dev/8x50x25/','best.pth.tar')
#args2 = dotdict({'numMCTSSims': 25, 'cpuct':1.0})
#mcts2 = MCTS(g, n2, args2)
#n2p = lambda x: np.argmax(mcts2.getActionProb(x, temp=0))

arena = Arena.Arena(n1p, g_player, g, display=display)
print(arena.playGames(2, verbose=True))
