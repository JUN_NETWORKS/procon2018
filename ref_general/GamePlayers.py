import numpy as np

# pit.pyないで使われているだけなのでしばらく手を付けなくてもいい感じかな?
class RandomPlayer():
    def __init__(self, game):
        self.game = game
        self.player_num = 0

    def play(self, board):
        a = np.random.randint(self.game.getActionSize())
        valids = self.game.getValidMoves(board, self.player_num)
        while valids[a]!=1:
            a = np.random.randint(self.game.getActionSize())
        return a


class HumanPlayer():
    def __init__(self, game):
        self.game = game
        self.player_num = 0

    def play(self, board):
        # display(board)
        valid = self.game.getValidMoves(board, self.player_num)
        for i in range(len(valid)):
            if valid[i]:
                print(int(i/self.game.n), int(i%self.game.n))
        while True:
            a = input()

            x,y = [int(x) for x in a.split(' ')]
            a = self.game.n * x + y if x!= -1 else self.game.n ** 2
            if valid[a]:
                break
            else:
                print('Invalid')

        return a

# pit.pyないで使われているだけなのでしばらく手を付けなくてもいい感じかな?
class GreedyPlayer():
    def __init__(self, game):
        self.game = game
        self.player_num = 0

    def play(self, board):
        valids = self.game.getValidMoves(board, self.player_num)
        candidates = []
        for a in range(self.game.getActionSize()):
            if valids[a]==0:
                continue
            nextBoard, _ = self.game.getNextState(board, self.player_num, a)
            score = self.game.getScore(nextBoard, self.player_num)
            candidates += [(-score, a)]
        candidates.sort()
        return candidates[0][1]
