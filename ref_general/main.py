from Coach import Coach
from Game import Game
from NNets.NNet import NNetWrapper as nn
from utils import *

args = dotdict({
    'numIters': 10,  # イテレーション回数
    'numEps': 25,  # プレイアウトする回数
    'tempThreshold': 10,  # mcts.getActionProb内でMCTSを実行せずにアクションを選ぶようになる閾値
    'updateThreshold': 0.6,  # 新モデルが旧モデルに対して勝率がtempThresholdでモデルsave
    'maxlenOfQueue': 200000,  # 過去の例の数
    'numMCTSSims': 10,  # MCTSシュミレーションを行う回数
    'arenaCompare': 40,
    'cpuct': 0.75,  # 探索の程度を制御するハイパーパラメータ UCTアルゴリズムのcの部分の定数

    'checkpoint': './temp/',
    'load_model': False,
    'load_folder_file': ('/dev/models/8x100x50','best.pth.tar'),
    'numItersForTrainExamplesHistory': 20,

})

if __name__=="__main__":
    g = Game()
    nnet = nn(g)

    if args.load_model:
        nnet.load_checkpoint(args.load_folder_file[0], args.load_folder_file[1])

    c = Coach(g, nnet, args)
    if args.load_model:
        print("Load trainExamples from file")
        c.loadTrainExamples()
    c.learn()
