#### Base Project

[alpha-zero-general](https://github.com/suragnair/alpha-zero-general)

### Reference
- Coach.py 内の同時手番ゲームにおけるゲーム木の考え方: [参考リンク](http://www.ocw.titech.ac.jp/index.php?module=General&action=DownLoad&file=201517701-35-0-25.pdf&type=cal&JWC=201517701)

# Board クラス

#### Player
White: 1<br>
Black: -1

input command :``((actionNum, (row,column)),(actionNum,(row,column)))``.<br>
example: ``((2, (1, 0)), (1, (-1, 1)))``<br>
This is meaning:
Agent1 action is delete tile and, direction is (1,0)
Agent2 action is move and, direction is (-1,1)
<br><br><br>
row and column is -1, 0, or 1. ex:(1,-1)


## self.board 変数のチャンネル

---
```self.board[:,:,channel]```

0 : マスの点数

1 : meが占拠しているマスが1になっているarray

2: enemy が占拠しているマスが1になっているarray

3: Agent white_1 の現在地

4: Agent white_2 の現在地

5: Agent black_1 の現在地

6: Agent black_2 の現在地

7: 本来のフィールドのサイズのマスが1のarray

8: 残りターン数