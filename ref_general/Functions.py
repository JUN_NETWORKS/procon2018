import numpy as np
from numba.decorators import jit


# 12*12の0配列に挿入
def insert_blank_array(field, return_diff=False):
    base = np.zeros((12, 12), dtype=int)

    row_diff = (base.shape[0] - field.shape[0]) // 2
    column_diff = (base.shape[1] - field.shape[1]) // 2

    # print(row_diff)
    # print(column_diff)

    for row in range(field.shape[0]):
        for column in range(field.shape[1]):
            base[row_diff + row][column_diff + column] = field[row][column]

    if return_diff:
        return base, (row_diff, column_diff)

    # 12*12の0配列に挿入した配列とずれ(agentsの座標修正用)を返す
    return base


# 領域ポイント計算の探索処理
@jit
def create_mask(possession_of_tile, max_rows, max_columns):
    """
    :param possession_of_tile: 自チーム占有マスを1で表したarray
    :return: 領域部分が0になったarray
    """
    # 自チームが占有したマスを除いた4辺を-1とする
    mask = np.copy(possession_of_tile)
    mask[[0, max_rows], :] = np.where(possession_of_tile[[0, max_rows], :] == 1, 1, -1)  # 0行目と最終行
    mask[:, [0, max_columns]] = np.where(possession_of_tile[:, [0, max_columns]] == 1, 1, -1)  # 0列目と最終列

    # record0 == record1 になる(領域の場所の確定)までループ
    record0, record1 = 0, 1
    equal = 0  # 前回と同じなら +1 する
    while equal < 10:
        record0 = record1
        for i in range(1, max_rows):
            for j in range(1, max_columns):

                around = []  # ある座標の周り(上下左右)の値を入れるリスト
                if mask[i][j] == 0:
                    around.append(mask[i + 1][j])
                    around.append(mask[i - 1][j])
                    around.append(mask[i][j + 1])
                    around.append(mask[i][j - 1])

                    if -1 in around:  # -1が周りにあった場合
                        mask[i][j] = -1
                    else:
                        pass
                else:  # 空白以外は無視
                    continue
        record1 = np.sum(mask == 0)
        if record0 == record1:
            equal += 1
        else:
            equal = 0

    return mask


# 行動パターン(256個)を作成
def get_action_patterns():
    agent_actions = []

    for action in [1, 2]:  # 1 for move, 2 for delete tile
        for direction in [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)]:  # All directions
            agent_actions.append((action, direction))

    agent1 = agent_actions
    agent2 = agent_actions

    actions = []
    for ag1 in agent1:
        for ag2 in agent2:
            actions.append((ag1, ag2))
    return actions


# score_fieldから自分のエージェント初期座標を作成
def generate_init_position(field_shape):
    white_1 = (np.random.randint(0, field_shape[0] // 2), np.random.randint(0, field_shape[1] // 2))
    # print(me_1)
    white_2 = (np.random.randint(np.ceil(field_shape[0] / 2), field_shape[0]),
               np.random.randint(np.ceil(field_shape[1] / 2), field_shape[1]))
    # print(me_2)

    return white_1, white_2


# 敵の初期座標を自分の初期座標を元に作成
def generate_black_position(white_1=(1, 1), white_2=(6, 9)):
    black_1 = (white_1[0], white_2[1])
    black_2 = (white_2[0], white_1[1])

    return black_1, black_2


# 2要素を持っているtuple同士の足し算. 座標計算で使う
def addition_of_tuple(tuple1, tuple2):
    return tuple(x + y for x, y in zip(tuple1, tuple2))


# 2要素を持っているtuple同士の引き算
def subtraction_of_tuple(tuple1, tuple2):
    return tuple(x - y for x, y in zip(tuple1, tuple2))


# 移動 -> 行動後の座標を返す
# 削除 -> エージェント自体は動かないので現在と同じ座標を返す
def move_or_delete(command, current, apply):
    if command == 1:
        return addition_of_tuple(current, apply)
    else:
        return current


# エージェントのChannel番号を取得
def get_agent_channel(player_num, agent_num):
    if player_num == 1:
        if agent_num == 1:
            return 3
        else:
            return 4
    else:
        if agent_num == 1:
            return 5
        else:
            return 6


# zeros配列の中に一つ1がある場合にその座標を返す
def get_one_in_2dzeros(arr):
    return np.where(arr == 1)
