import numpy as np

"""
フィールド自動生成
"""


# 0以下の数の出現率が10%の乱数生成器
def generate_point():
    weight = np.ones(33)
    weight[:17] = 1 / 17 * 0.1  # 0点以下の点数出現率10%を0以下の数字に均等に振る
    weight[17:] = 1 / 16 * 0.9  # 1点以上の点数出現率90%を1以上の数字に均等に振る

    return np.random.choice(range(-16, 17), p=weight)


# 行,列を渡すとフィールド生成
def create_field(height, width, v_symmetry, h_symmetry):
    """
    :param height: 縦
    :param width: 横
    :param v_symmetry: 縦対称
    :param h_symmetry: 横対称
    :return: 生成したフィールドの array
    """
    if width * height > 12 * 12:
        print("width*height > 12*12")
    data = np.zeros((height, width), dtype=np.int)
    for i_width in range(width // 2):
        for i_height in range(height // 2):
            rand_value = generate_point()
            data[i_height][i_width] = rand_value

            # 横のみ対称
            if h_symmetry == True and v_symmetry == False:
                data[i_height][width - i_width - 1] = rand_value
                rand_value = generate_point()
                data[height - i_height - 1][i_width] = rand_value
                data[height - i_height - 1][width - i_width - 1] = rand_value

            # 縦のみ対称
            if h_symmetry == False and v_symmetry == True:
                data[height - i_height - 1][i_width] = rand_value
                rand_value = generate_point()
                data[i_height][width - i_width - 1] = rand_value
                data[height - i_height - 1][width - i_width - 1] = rand_value

            # 縦横の両方が対称
            if h_symmetry == True and v_symmetry == True:
                data[height - i_height - 1][i_width] = rand_value
                data[i_height][width - i_width - 1] = rand_value
                data[height - i_height - 1][width - i_width - 1] = rand_value

    # 縦が奇数
    # 中央の行だけ別で処理する
    if height % 2 == 1:
        for i_width in range(width // 2):
            rand_value = generate_point()
            data[height // 2][i_width] = rand_value
            # 横のみ対称
            if h_symmetry == True:
                data[height // 2][width - i_width - 1] = rand_value
            else:
                data[height // 2][width - i_width - 1] = generate_point()

    # 横が奇数
    # print(" "+str(math.ceil(width/2))+" "+str(math.floor(width/2)))
    if width % 2 == 1:
        # center tile point
        data[height//2][width//2] = generate_point()

        for i_height in range(height // 2):
            rand_value = generate_point()
            data[i_height][width // 2] = rand_value
            if h_symmetry == True:
                data[height - i_height - 1][width // 2] = rand_value
            else:
                data[height - i_height - 1][width // 2] = generate_point()
        if (height // 2 - height // 2) > 0:
            data[height // 2][width // 2] = generate_point()

    # return: field_array
    return data


# 行,列をランダムで決めてフィールド生成
def generate_field():
    # 行を決める
    row = np.random.randint(7, 13)
    # 80マス以上になるように列を決める
    min_col = np.ceil(80 / row)
    column = np.random.randint(min_col, 13)

    # v_symmetry & h_symmetry
    v_symmetry = np.random.choice([True, False])
    # 必ず水平もしくは垂直に対称
    if v_symmetry == False:
        h_symmetry = True
    else:
        h_symmetry = np.random.choice([True, False])

    # print("v: {}\nh: {}".format(v_symmetry, h_symmetry))

    # generate field
    return create_field(row, column, v_symmetry, h_symmetry)
